/**
 * Copyright (c) 2016 Dmitry Soloma(flyingmeatsoul@gmail.com).
 * https://bitbucket.org/dmitry_soloma/model-form
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
HTMLElement.prototype.setValue = function (value) {
    switch (this.type) {
        case 'radio':
        case 'checkbox':
            if (value === null) {
                this.removeAttribute('checked');
                break;
            }
            if (this.value == value) {
                this.setAttribute('checked', 'checked');
            } else {
                this.removeAttribute('checked');
            }
            break;
        case 'select-one':
            for (var i = 0; i < this.length; i++) {
                if (this.options[i].value == value) {
                    this.options[i].setAttribute('selected', 'selected');
                } else {
                    this.options[i].removeAttribute('selected');
                }
            }
            break;
        case 'select-multiple':
            if (value == null) {
                for (var i = 0; i < this.length; i++) {
                    this.options[i].removeAttribute('selected');
                }
                break;
            }
            var values = value.split('|');
            for (var i = 0; i < this.length; i++) {
                if (values.indexOf(this.options[i].value) !== -1) {
                    this.options[i].setAttribute('selected', 'selected');
                } else {
                    this.options[i].removeAttribute('selected');
                }
            }
            break;
        default:
            this.value = value;
    }

    return this;
};
HTMLElement.prototype.getValue = function () {
    var value = null;
    switch (this.type) {
        case 'radio':
        case 'checkbox':
            if (this.hasOwnProperty('checked') || this.checked) {
                value = this.value;
            }
            break;
        case 'select-one':
            for (var i = 0; i < this.length; i++) {
                if (this.options[i].selected) {
                    value = this.options[i].value;
                }
            }
            break;
        case 'select-multiple':
            var values = [];
            for (var i = 0; i < this.length; i++) {
                if (this.options[i].selected) {
                    values.push(this.options[i].value);
                }
            }
            value = values.join('|');
            break;
        default:
            value = this.value;
    }

    return value;
};

var Form;
Form = function (form) {
    var _data = {};
    var _eventDispatcher = new EventDispatcher();
    var _exts = {};

    (function () {
        _each(function (element) {
            _updateData(element);
            element.addEventListener('change', function (event) {
                _updateData(event.target);
            });
            element.addEventListener('keyup', function (event) {
                _updateData(event.target);
            });
        });
        _exts['validator'] = new Validator(form);
    })();

    function _updateData(element) {
        var name = element.name;
        var value = element.getValue();
        var result = name.match(/\[\w+\]/ig);
        if (result) {
            for (var i = 0; i < result.length; i++) {
                result[i] = result[i].replace(/\[|\]/g, '');
            }
            var attribute = result.join('/');
            _eventDispatcher.dispatch('beforeChange', _data, attribute);
            _data = _merge(_data, _build(result, value));
            _eventDispatcher.dispatch('afterChange', _data, attribute);
        }
    }

    function _merge(source, target) {
        for (var prop in target) {
            if (typeof target[prop] == 'object' && target[prop] != null) {
                if (!source.hasOwnProperty(prop) || typeof source[prop] != 'object') {
                    source[prop] = {};
                }
                source[prop] = _merge(source[prop], target[prop]);
            } else {
                source[prop] = target[prop];
            }
        }

        return source;
    }

    function _build(keys, value) {
        var data = {};
        if (!keys) {
            return null;
        }
        var key = keys.shift();
        if (!key) {
            return value;
        }
        if (keys.length > 0) {
            data[key] = _build(keys, value);
        } else {
            data[key] = value;
        }

        return data;
    }

    function _get(key) {
        if (typeof key == 'undefined') {
            return _data;
        }
        if (_data.hasOwnProperty(key)) {
            return _data[key];
        }
        var regExp = new RegExp('\/', 'g');
        if (regExp.test(key)) {
            var keys = key.split('/');
            return _getRecursive(keys, _data);
        }

        return null;
    }

    function _getRecursive(keys, source) {
        var key = keys.shift();
        if (source.hasOwnProperty(key)) {
            if (typeof source[key] == 'object' && keys.length > 0) {
                return _getRecursive(keys, source[key]);
            } else {
                if (keys.length > 0) {
                    return null;
                }
                return source[key];
            }
        }

        return null;
    }

    function _set(key, value) {
        if (typeof key == 'object') {
            Object.keys(key).forEach(function (index) {
                _eventDispatcher.dispatch('beforeChange', _data, index);
                var data = {};
                value = key[index];
                var regExp = new RegExp('\/', 'g');
                if (regExp.test(index)) {
                    var keys = index.split('/');
                    var name = keys.join('\\]\\[');
                    data[name] = value;
                    _data = _merge(_data, _build(keys, value));
                } else {
                    _data[index] = value;
                    data[index] = value;
                }
                _fill(data);
                _eventDispatcher.dispatch('afterChange', _data, index);
            });
        } else {
            var data = {};
            _eventDispatcher.dispatch('beforeChange', _data, key);
            var regExp = new RegExp('\/', 'g');
            if (regExp.test(key)) {
                var keys = key.split('/');
                var name = keys.join('\\]\\[');
                data[name] = value;
                _data = _merge(_data, _build(keys, value));
            } else {
                _data[key] = value;
                data[key] = value;
            }
            _fill(data);
            _eventDispatcher.dispatch('afterChange', _data, key);
        }
    }

    function _each(callback) {
        for (var i = 0; i < form.length; i++) {
            var formElement = form.elements[i];
            callback(formElement, i);
        }
    }

    function _getPaths(data, key) {
        if (!data) {
            data = _data;
        }
        if (!key) {
            key = '';
        } else {
            key = key + '/';
        }
        var paths = [];
        Object.keys(data).forEach(function (index) {
            var path = key + index;
            if (typeof data[index] == 'object' && data[index] != null) {
                paths = paths.concat(_getPaths(data[index], path));
            } else {
                paths.push(path);
            }
        });

        return paths;
    }

    function _fill(data) {
        Object.keys(data).forEach(function (key) {
            _each(function (element) {
                var rxSearchName = new RegExp(key, 'ig');
                if (rxSearchName.test(element.name)) {
                    element.setValue(data[key]);
                }
            });
        });
    }

    function _reset() {
        var paths = _getPaths();
        paths.forEach(function (key) {
            _set(key, null);
        });
    }

    return {
        model: function () {
            return {
                get: function (key) {
                    return _get(key);
                },
                set: function (key, value) {
                    _set(key, value);
                    return this;
                },
                reset: function () {
                    _reset();
                    return this;
                },
                observe: function (event, callback) {
                    _eventDispatcher.observe(event, callback);
                    return this;
                },
                dispatch: function (event, target, attribute) {
                    _eventDispatcher.dispatch(event, target, attribute);
                    return this;
                },
                clearObserver: function (event) {
                    _eventDispatcher.removeEventCallback(event);
                    return this;
                }
            }
        },
        elements: function () {
            return {
                each: function (callback) {
                    _each(callback);

                    return this;
                },
                disable: function () {
                    _each(function (element) {
                        element.disabled = true;
                    });

                    return this;
                },
                enable: function () {
                    _each(function (element) {
                        element.disabled = false;
                    });

                    return this;
                },
                toggle: function () {
                    _each(function (element) {
                        element.disabled = !element.disabled;
                    });

                    return this;
                },
                reset: function () {
                    _each(function (element) {
                        element.setValue(null);
                    });

                    return this;
                }
            }
        },
        extension: function () {
            return {
                add: function (name, extension) {
                    _exts[name] = extension;

                    return this;
                },
                get: function (name) {
                    if (_exts.hasOwnProperty(name)) {
                        return _exts[name];
                    }

                    return null;
                },
                remove: function (name) {
                    if (_exts.hasOwnProperty(name)) {
                        delete _exts[name];
                    }

                    return this;
                }
            };
        }
    };
};

var EventDispatcher = function () {
    var _eventCallback = {};
    return {
        dispatch: function (event, target, attribute) {
            for (var prop in _eventCallback) {
                if (event == prop) {
                    for (var i = 0; i < _eventCallback[event].length; i++) {
                        _eventCallback[event][i](target, attribute);
                    }
                }
            }

            return this;
        },
        observe: function (event, callback) {
            if (_eventCallback.hasOwnProperty(event)) {
                _eventCallback[event].push(callback);
            } else {
                _eventCallback[event] = [];
                _eventCallback[event].push(callback);
            }

            return this;
        },
        removeEventCallback: function (event) {
            if (typeof event == 'undefined') {
                _eventCallback = {};
            } else {
                if (_eventCallback.hasOwnProperty(event)) {
                    delete _eventCallback[event];
                }
            }

            return this;
        }
    }
}

var Validator = function (form) {
    var _controls = form.elements;
    var _errors = [];
    var _rules = {
        'not-empty': function (value) {
            if (value.length == 0) {
                return 'Can not be en empty';
            }

            return false;
        }
    };
    return {
        check: function (name, value) {
            if (_rules.hasOwnProperty(name)) {
                return _rules[name](value);
            }

            return false;
        },
        addRule: function (name, callback) {
            if (typeof name != 'undefined') {
                _rules[name] = callback;
            }

            return this;
        },
        removeRule: function (name) {
            if (_rules.hasOwnProperty(name)) {
                delete _rules[name];
            }

            return this;
        },
        clearRules: function () {
            _rules = {};

            return this;
        },
        clearErrors: function () {
            for (var i = 0; i < _controls.length; i++) {
                _controls[i].removeAttribute('data-error');
            }
            _errors = [];

            return this;
        },
        runValidation: function () {
            this.clearErrors();
            var success = true;
            for (var i = 0; i < _controls.length; i++) {
                if (!this.validate(_controls[i])) {
                    success = false;
                }
            }

            return success;
        },
        validate: function (element) {
            var validation = element.dataset.validation;
            if (typeof validation == 'undefined') {
                return true;
            }
            var validators = validation.split(' ');
            var value = element.getValue();
            if (!validators.length > 0) {
                return true;
            }
            for (var i = 0; i < validators.length; i++) {
                var result;
                result = this.check(validators[i], value);
                if (result) {
                    element.setAttribute('data-error', result);
                    _errors.push(result);

                    return false;
                }
            }

            return true;
        }
    }
};
